package wynners.mlmarketplace

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.ml.v1.CloudMachineLearningEngine
import com.google.api.services.ml.v1.model.GoogleCloudMlV1Job
import com.google.api.services.ml.v1.model.GoogleCloudMlV1Model
import com.google.api.services.ml.v1.model.GoogleCloudMlV1PredictionInput
import com.google.api.services.ml.v1.model.GoogleCloudMlV1Version
import com.google.auth.oauth2.GoogleCredentials
import com.google.cloud.storage.Bucket
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.google.firebase.cloud.StorageClient
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.web.SpringServletContainerInitializer
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController
import java.nio.channels.Channels
import java.util.zip.ZipInputStream

private val PROJECT_ID = "projects/unihack2019-234701"

@SpringBootApplication
@RestController
class MlMarketplaceApplication : SpringServletContainerInitializer() {
    val firebaseBucket: Bucket
    val ml_engine_models: CloudMachineLearningEngine.Projects.Models
    val ml_engine_versions: CloudMachineLearningEngine.Projects.Models.Versions
    val ml_engine_jobs : CloudMachineLearningEngine.Projects.Jobs

    init {
        FirebaseApp.initializeApp(
            FirebaseOptions.Builder().
                setCredentials(GoogleCredentials.getApplicationDefault()).
                setStorageBucket("unihack2019-234701.appspot.com")
                .build()
        )
        firebaseBucket = StorageClient.getInstance().bucket()
        CloudMachineLearningEngine.Builder(
            GoogleNetHttpTransport.newTrustedTransport(),
            JacksonFactory.getDefaultInstance(),
            GoogleCredential.getApplicationDefault()
        ).build().projects().run {
            ml_engine_models = models().apply { ml_engine_versions = versions() }
            ml_engine_jobs = jobs()
        }
    }

    @RequestMapping("/")
    fun index(): String {
        return "Hello from ML Marketplace Backend Server"
    }

    @RequestMapping("/{pid}/{sid}/create", method = [RequestMethod.POST])
    fun createModel(@PathVariable pid: String, @PathVariable sid: String) {
        val mnm = "$pid/$sid/models"
        val mn = "$pid$sid"
        unzip(mnm, "$mn")
        ml_engine_models.create(PROJECT_ID, GoogleCloudMlV1Model().setName(mn)).execute()
        ml_engine_versions.create(
            "$PROJECT_ID/models/$mn",
            GoogleCloudMlV1Version().setName("default").setDeploymentUri("gs://${firebaseBucket.name}/$mnm")
        ).execute()
    }

    @RequestMapping("/{pid}/{sid}/predict", method = [RequestMethod.POST])
    fun modelPredict(@PathVariable pid: String, @PathVariable sid: String, inputFilenames: List<String>) {
        val input = "gs://${firebaseBucket.name}/input/$pid"
        ml_engine_jobs.create(
            PROJECT_ID,
            GoogleCloudMlV1Job().setPredictionInput(
                GoogleCloudMlV1PredictionInput().setModelName("$pid$sid").setInputPaths(
                    inputFilenames.map { "$input/$it" }
                ).setOutputPath("gs://${firebaseBucket.name}/output/$pid/$sid.out")
            )
        ).execute()
    }

    @RequestMapping("/{pid}/{sid}")
    fun rate(@PathVariable pid: String, @PathVariable sid: String, labelFile: String): Double {
        return 0.0
    }

    private fun unzip(unzipPath: String, zipFile: String) {
        val b = firebaseBucket.get(zipFile)
        val z = ZipInputStream(Channels.newInputStream(b.reader()))
        while (z.nextEntry?.also { firebaseBucket.create("$unzipPath/${it.name}", z.readBytes()) } != null);
        b.delete()
    }
}

fun main(args: Array<String>) {
    runApplication<MlMarketplaceApplication>(*args)
}
