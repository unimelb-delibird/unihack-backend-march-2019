package wynners.mlmarketplace.rate

interface Rater<T> {
    fun rate(predictions: List<T>, labels: List<T>): Double
}

fun <T> getRater(name: String): Rater<T> {
    return when (name) {
        "accuracy" -> AccuracyRater()
        else -> TrivialRater()
    }
}

private class TrivialRater <T> : Rater<T> {
    override fun rate(predictions: List<T>, labels: List<T>): Double {
        return 0.0
    }
}

private class AccuracyRater <T> : Rater<T> {
    override fun rate(predictions: List<T>, labels: List<T>): Double {
        return predictions.zip(labels) { l, r -> l == r }.filter { it }.size * 1.0 / predictions.size
    }
}
